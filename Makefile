all: symmetric asymmetric

symmetric: symmetric.c
	gcc -lsodium -o symmetric symmetric.c

asymmetric: asymmetric.c
	gcc -lsodium -o asymmetric asymmetric.c

clean:
	rm -f asymmetric symmetric
