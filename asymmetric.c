#include <stdlib.h>
#include <sodium.h>
#include <string.h>

void usage() {
	printf("Usage:\n");
	printf("\tKey generation:\n\t\t ./asymmetric -g [keyname]\n");
	printf("\tEncryption/Decryption:\n\t\t ./asymmetric -[e|d] [secret key file] [public key file] [nonce] [input file] [output file]\n");
}

int main(int argc, char const *argv[]) {
	if (argc < 2) {
		usage();
		return EXIT_FAILURE;
	}

	char mode = argv[1][1];
	if (mode != 'g' && mode != 'd' && mode != 'e') {
		fprintf(stderr, "Error: invalid mode\n");
		return EXIT_FAILURE;
	}

	if (sodium_init() < 0) {
		fprintf(stderr, "Error: sodium init failed\n");
		return EXIT_FAILURE;
	}

	if (mode == 'g') {
		if (argc < 3) {
			usage();
			return EXIT_FAILURE;
		}

		const char * filename = argv[2];

		unsigned char pk[crypto_box_PUBLICKEYBYTES];
		unsigned char sk[crypto_box_SECRETKEYBYTES];
		crypto_box_keypair(pk, sk);

		char pubfilename[strlen(filename) + 8];
		strcpy(pubfilename, filename);
		strcat(pubfilename, ".public");

		char secfilename[strlen(filename) + 8];
		strcpy(secfilename, filename);
		strcat(secfilename, ".secret");

		FILE * pkfile = fopen(pubfilename, "w");
		fwrite(pk, 1, crypto_box_PUBLICKEYBYTES, pkfile);

		FILE * skfile = fopen(secfilename, "w");
		fwrite(sk, 1, crypto_box_SECRETKEYBYTES, skfile);

	} else {

		if (argc < 7) {
			usage();
			return EXIT_FAILURE;
		}

		const char * secfilename = argv[2];
		const char * pubfilename = argv[3];

		unsigned char nonce[24];
		strncpy((char *)nonce, argv[4], 24);

		const char * inputfilename = argv[5];
		const char * outputfilename = argv[6];

		unsigned char sk[crypto_box_SECRETKEYBYTES];
		unsigned char pk[crypto_box_PUBLICKEYBYTES];

		FILE * secfile = fopen(secfilename, "r");
		FILE * pubfile = fopen(pubfilename, "r");

		fread(sk, 1, crypto_box_SECRETKEYBYTES, secfile);
		fread(pk, 1, crypto_box_PUBLICKEYBYTES, pubfile);

		FILE * input = fopen(inputfilename, "r");
		fseek(input, 0L, SEEK_END);
		size_t filesize = ftell(input);
		rewind(input);

		unsigned char * inputbuf = malloc(filesize);

		size_t inputsize = fread(inputbuf, 1, filesize, input);

		size_t outputsize = 0;

		if (mode == 'e') {
			outputsize = inputsize + crypto_secretbox_MACBYTES;
		} else if (mode == 'd') {
			outputsize = inputsize - crypto_secretbox_MACBYTES;
		}

		unsigned char * outputbuf = malloc(outputsize);

		if (mode == 'e') {
			if (crypto_box_easy(outputbuf, inputbuf, inputsize, nonce, pk, sk) != 0) {
				fprintf(stderr, "Error: encryption failed\n");
				return EXIT_FAILURE;
			}
		} else if (mode == 'd') {
			if (crypto_box_open_easy(outputbuf, inputbuf, inputsize, nonce, pk, sk) != 0) {
				fprintf(stderr, "Error; decryption failed\n");
				return EXIT_FAILURE;
			}
		}

		FILE * output = fopen(outputfilename, "w");
		fwrite(outputbuf, outputsize, 1, output);


	}
	return EXIT_SUCCESS;
}
