#include <stdlib.h>
#include <sodium.h>
#include <string.h>

void usage(int status) {
	printf("Usage:\n");
	printf("\t ./symmetric -[d|e] [KEY] [NONCE] [input file] [output file]\n");
	exit(status);
}

int main(int argc, char const *argv[]) {
	if (argc < 6) usage(EXIT_FAILURE);

	char mode = argv[1][1];
	if (mode != 'd' && mode != 'e') {
		fprintf(stderr, "Error: mode not recognized\n");
		return EXIT_FAILURE;
	}

	unsigned char key[32];
	strncpy((char *)key, argv[2], 32);

	unsigned char nonce[24];
	strncpy((char *)nonce, argv[3], 24);

	const char * inputfile = argv[4];
	const char * outputfile = argv[5];


	if (sodium_init() < 0) {
		fprintf(stderr, "Error: sodium init failed\n");
		return EXIT_FAILURE;
	}

	FILE * input = fopen(inputfile, "r");
	fseek(input, 0L, SEEK_END);
	size_t filesize = ftell(input);
	rewind(input);

	unsigned char * inputbuf = malloc(filesize);

	size_t inputsize = fread(inputbuf, 1, filesize, input);

	size_t outputsize = 0;

	if (mode == 'e') {
		outputsize = inputsize + crypto_secretbox_MACBYTES;
	} else if (mode == 'd') {
		outputsize = inputsize - crypto_secretbox_MACBYTES;
	}

	unsigned char * outputbuf = malloc(outputsize);

	if (mode == 'e') {
		if (crypto_secretbox_easy(outputbuf, inputbuf, inputsize, nonce, key) != 0) {
			fprintf(stderr, "Error: encryption failed\n");
			return EXIT_FAILURE;
		}
	} else if (mode == 'd') {
		if (crypto_secretbox_open_easy(outputbuf, inputbuf, inputsize, nonce, key) != 0) {
			fprintf(stderr, "Error; decryption failed\n");
			return EXIT_FAILURE;
		}
	}

	FILE * output = fopen(outputfile, "w");
	fwrite(outputbuf, outputsize, 1, output);

	free(inputbuf);
	free(outputbuf);

	return EXIT_SUCCESS;
}
